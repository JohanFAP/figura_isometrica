import pygame
from libreria import *

if __name__ == '__main__':
    pygame.init()
    pantalla = pygame.display.set_mode([ANCHO, ALTO])

    centro = [400,600]
    a = [270,0]
    b = [90,0]
    c = [90,0]
    d = [180,0]
    e = [90,0]
    f = [180,0]
    g = [90,0]

    a_r = Rotacion(a,30)
    a_rc = Cart_Pant(a_r, centro)
    b_r = Rotacion(b,90)
    b_rc = Cart_Pant(b_r, a_rc)
    c_r = Rotacion(c,210)
    c_rc = Cart_Pant(c_r, b_rc)
    d_r = Rotacion(d,90)
    d_rc = Cart_Pant(d_r, c_rc)
    e_r = Rotacion(e,210)
    e_rc = Cart_Pant(e_r, d_rc)
    f_r = Rotacion(f,270)
    f_rc = Cart_Pant(f_r, e_rc)
    g_r = Rotacion(g,210)
    g_rc = Cart_Pant(g_r, f_rc)

    l1 = [centro,a_rc,b_rc,c_rc,d_rc,e_rc,f_rc,g_rc] #F BLANCO

    a2 = [270,0]
    b2 = [90,0]
    c2 = [270,0]

    a2_r = Rotacion(a2,150)
    a2_rc = Cart_Pant(a2_r, centro)
    b2_r = Rotacion(b2,90)
    b2_rc = Cart_Pant(b2_r, a2_rc)
    c2_r = Rotacion(c2,330)
    c2_rc = Cart_Pant(c2_r, b2_rc)

    l2 = [centro,a2_rc,b2_rc,c2_rc] #L1 ROJO

    a3 = [270,0]
    b3 = [180,0]
    c3 = [270,0]

    a3_r = Rotacion(a3,150)
    a3_rc = Cart_Pant(a3_r, f_rc)
    b3_r = Rotacion(b3,90)
    b3_rc = Cart_Pant(b3_r, a3_rc)
    c3_r = Rotacion(c3,330)
    c3_rc = Cart_Pant(c3_r, b3_rc)

    l3 = [f_rc,a3_rc,b3_rc,c3_rc] #L2 ROJO

    a4 = [270,0]
    b4 = [90,0]
    c4 = [270,0]

    a4_r = Rotacion(a4,150)
    a4_rc = Cart_Pant(a4_r, g_rc)
    b4_r = Rotacion(b4,30)
    b4_rc = Cart_Pant(b4_r, a4_rc)
    c4_r = Rotacion(c4,330)
    c4_rc = Cart_Pant(c4_r, b4_rc)

    l4 = [g_rc,a4_rc,b4_rc,c4_rc] #S1 AZUL

    a5 = [270,0]
    b5 = [90,0]
    c5 = [270,0]

    a5_r = Rotacion(a5,150)
    a5_rc = Cart_Pant(a5_r, e_rc)
    b5_r = Rotacion(b5,30)
    b5_rc = Cart_Pant(b5_r, a5_rc)
    c5_r = Rotacion(c5,330)
    c5_rc = Cart_Pant(c5_r, b5_rc)

    l5 = [e_rc,a5_rc,b5_rc,c5_rc] #S2 AZUL

    a6 = [270,0]
    b6 = [90,0]
    c6 = [270,0]

    a6_r = Rotacion(a6,150)
    a6_rc = Cart_Pant(a6_r, c_rc)
    b6_r = Rotacion(b6,30)
    b6_rc = Cart_Pant(b6_r, a6_rc)
    c6_r = Rotacion(c6,330)
    c6_rc = Cart_Pant(c6_r, b6_rc)

    l6 = [c_rc,a6_rc,b6_rc,c6_rc] #S3 AZUL

    def Trasladar(l1,l2,l3,l4,l5,l6,pix):
        ls_v1T = [Traslacion(punto,pix) for punto in l1]
        ls_v2T = [Traslacion(punto,pix) for punto in l2]
        ls_v3T = [Traslacion(punto,pix) for punto in l3]
        ls_v4T = [Traslacion(punto,pix) for punto in l4]
        ls_v5T = [Traslacion(punto,pix) for punto in l5]
        ls_v6T = [Traslacion(punto,pix) for punto in l6]
        return ls_v1T,ls_v2T,ls_v3T,ls_v4T,ls_v5T,ls_v6T

    def Escalar(l1,l2,l3,l4,l5,l6,s):
        ls_v1E = [Escala(punto,[s,s]) for punto in l1]
        ls_v2E = [Escala(punto,[s,s]) for punto in l2]
        ls_v3E = [Escala(punto,[s,s]) for punto in l3]
        ls_v4E = [Escala(punto,[s,s]) for punto in l4]
        ls_v5E = [Escala(punto,[s,s]) for punto in l5]
        ls_v6E = [Escala(punto,[s,s]) for punto in l6]
        return ls_v1E,ls_v2E,ls_v3E,ls_v4E,ls_v5E,ls_v6E

    def EscalarCentro(c,l1,l2,l3,l4,l5,l6,s):
        pf = [-1*c[0],-1*c[1]]
        l1,l2,l3,l4,l5,l6 = Trasladar(l1,l2,l3,l4,l5,l6,pf)
        l1,l2,l3,l4,l5,l6 = Escalar(l1,l2,l3,l4,l5,l6,s)
        l1,l2,l3,l4,l5,l6 = Trasladar(l1,l2,l3,l4,l5,l6,c)
        return l1,l2,l3,l4,l5,l6

    def Rotar(l1,l2,l3,l4,l5,l6,g):
        ls_v1E = [Rotacion(punto,g) for punto in l1]
        ls_v2E = [Rotacion(punto,g) for punto in l2]
        ls_v3E = [Rotacion(punto,g) for punto in l3]
        ls_v4E = [Rotacion(punto,g) for punto in l4]
        ls_v5E = [Rotacion(punto,g) for punto in l5]
        ls_v6E = [Rotacion(punto,g) for punto in l6]
        return ls_v1E,ls_v2E,ls_v3E,ls_v4E,ls_v5E,ls_v6E

    def RotarCentro(c,l1,l2,l3,l4,l5,l6,g):
        pf = [-1*c[0],-1*c[1]]
        l1,l2,l3,l4,l5,l6 = Trasladar(l1,l2,l3,l4,l5,l6,pf)
        l1,l2,l3,l4,l5,l6 = Rotar(l1,l2,l3,l4,l5,l6,g)
        l1,l2,l3,l4,l5,l6 = Trasladar(l1,l2,l3,l4,l5,l6,c)
        return l1,l2,l3,l4,l5,l6

    def Dibujar(l1,l2,l3,l4,l5,l6):
        pantalla.fill(NEGRO)
        pygame.draw.polygon(pantalla, AZUL, l6)
        pygame.draw.polygon(pantalla, AZUL, l5)
        pygame.draw.polygon(pantalla, AZUL, l4)
        pygame.draw.polygon(pantalla, ROJO, l3)
        pygame.draw.polygon(pantalla, ROJO, l2)
        pygame.draw.polygon(pantalla, BLANCO, l1)
        pygame.display.flip()
        return None

    Dibujar(l1,l2,l3,l4,l5,l6)
    l1_t = []
    l2_t = []
    l3_t = []
    l4_t = []
    l5_t = []
    l6_t = []
    pix=[0,0]
    s = 1
    g = 0
    fin = False
    while not fin:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RIGHT:
                    pix[0] += 10

                if event.key == pygame.K_LEFT:
                    pix[0] -= 10

                if event.key == pygame.K_UP:
                    pix[1] -= 10

                if event.key == pygame.K_DOWN:
                    pix[1] += 10

            if event.type == pygame.MOUSEBUTTONDOWN:
                if event.button == 4:
                    s += 0.1

                if event.button == 5:
                    s -= 0.1

                if event.button == 1:
                    g -= 5

                if event.button == 3:
                    g += 5

            l1_t,l2_t,l3_t,l4_t,l5_t,l6_t = Trasladar(l1,l2,l3,l4,l5,l6,pix)
            centro = l1_t[0]
            l1_t,l2_t,l3_t,l4_t,l5_t,l6_t = EscalarCentro(centro,l1_t,l2_t,l3_t,l4_t,l5_t,l6_t,s)
            l1_t,l2_t,l3_t,l4_t,l5_t,l6_t = RotarCentro(centro,l1_t,l2_t,l3_t,l4_t,l5_t,l6_t,g)
            Dibujar(l1_t,l2_t,l3_t,l4_t,l5_t,l6_t)

    pygame.quit()