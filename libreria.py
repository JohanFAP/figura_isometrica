import math

PURPURA = [200,100,200]
VERDE = [0,255,0]
ROJO = [255,0,0]
AZUL = [0,0,255]
BLANCO = [255,255,255]
NEGRO = [0,0,0]
ANCHO = 800
ALTO = 800

def Cart_Pant(p, centro):
    '''
    p=[x,y] x=p[0] y=p[1]
    centro=[centro_x, centro_y] centro_x=centro[0], centro_y=centro[1]
    '''
    xp = p[0] + centro[0]
    yp = -p[1] + centro[1]
    return [xp,yp]

def Escala(p,s):
    xp=p[0]*s[0]
    yp=p[1]*s[1]
    return [xp,yp]

def Traslacion(p,t):
    xp=p[0]+t[0]
    yp=p[1]+t[1]
    return [xp,yp]

#Rotacion Antihoraria
def Rotacion(p, angulo):
    a_rad=math.radians(angulo)
    xp= (p[0]*math.cos(a_rad)) - (p[1]*math.sin(a_rad))
    yp = (p[0]*math.sin(a_rad)) + (p[1]*math.cos(a_rad))
    return [int(xp), int(yp)]

#Rotacion Horaria
def Rotacion_Hor(p, angulo):
    a_rad=math.radians(angulo)
    xp = (p[0]*math.cos(a_rad)) + (p[1]*math.sin(a_rad))
    yp = (p[1]*math.cos(a_rad)) - (p[0]+math.sin(a_rad))
    return [int(xp), int(yp)]